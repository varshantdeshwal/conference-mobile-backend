const express = require("express");
const router = express.Router();
const BookingUtil = require("../utils/bookingUtil");
const checkAuth = require("../auth/check-auth");

router.get("/:roomId/:date", checkAuth, (req, res, next) => {
  BookingUtil.getBookedSlots(
    { roomId: req.params.roomId, date: req.params.date },
    (err, doc) => {
      if (err) {
        res.status(500).json({
          error: err
        });
      }
      var slots = [];
      for (let i = 0; i < doc.length; i++) {
        slots = slots.concat(doc[i].slots);
      }
      res.status(200).json(slots);
    }
  );
});
router.get("/:email", checkAuth, (req, res, next) => {
  BookingUtil.getBookings({ email: req.params.email }, (err, doc) => {
    if (err) {
      res.status(500).json({
        error: err
      });
    }

    res.status(200).json(doc);
  });
});
router.delete("/", checkAuth, (req, res, next) => {
  BookingUtil.deleteBookings({ _id: { $in: req.body.bookings } }, err => {
    if (err) {
      console.log(err);
      res.status(500).json({
        error: err
      });
    }
    res.status(200).json({ message: "deleted successfully" });
  });
});
router.delete("/:email", checkAuth, (req, res, next) => {
  BookingUtil.deleteBookings({ email: req.params.email}, err => {
    if (err) {
      console.log(err);
      res.status(500).json({
        error: err
      });
    }
    res.status(200).json({ message: "deleted successfully" });
  });
});
router.post("/", checkAuth, (req, res) => {
  BookingUtil.checkBooking(
    { roomId: req.body.roomId, date: req.body.date },
    "slots",
    (err, doc) => {
      if (err) {
        res.status(500).json({
          error: err
        });
      } else if (doc.length >= 1) {
        var slots = [];
        for (let i = 0; i < doc.length; i++) {
          slots = slots.concat(doc[i].slots);
        }
        var found = req.body.slots.some(s => slots.includes(s));
        if (found) {
          res.status(200).json({ message: "already booked" });
        } else {
          BookingUtil.addBooking(req.body, (err, doc) => {
            if (err) {
              res.status(500).json({
                error: err
              });
            }
            res.status(200).json({ message: "booking done", doc });
          });
        }
      } else {
        BookingUtil.addBooking(req.body, (err, doc) => {
          if (err) {
            res.status(500).json({
              error: err
            });
          }
          res.status(200).json({ message: "booking done", doc });
        });
      }
    }
  );
});
module.exports = router;
