const express = require("express");
const router = express.Router();
const ClientUtil = require("../utils/clientUtil");
const checkAuth = require("../auth/check-auth");
const Client = require("../models/client");
router.get("/:devId", checkAuth, (req, res, next) => {
  ClientUtil.getClients({ devId: req.params.devId }, (err, doc) => {
    if (err) {
      console.log(err);
      res.status(500).json({
        error: err
      });
    }
    res.status(200).json(doc);
  });
});

router.post("/", checkAuth, (req, res, next) => {
  ClientUtil.checkClient(req.body, (err, doc) => {
    if (err) {
      console.log(err);
      res.status(500).json({
        error: err
      });
    } else if (doc.length >= 1) {
      res.status(400).json({ message: "client already exists" });
    } else {
      ClientUtil.addClient(req.body, (err, doc) => {
        if (err) {
          console.log(err);
          res.status(500).json({
            error: err
          });
        }
        res.status(201).json([doc, { message: "client created" }]);
      });
    }
  });
});

router.put("/:id", checkAuth, (req, res, next) => {
  ClientUtil.checkClient(req.body, (err, doc) => {
    if (err) {
      console.log(err);
      res.status(500).json({
        error: err
      });
    } else if (doc.length >= 1) {
      res.status(400).json({ message: "client already exists" });
    } else {
      ClientUtil.updateClient({ _id: req.params.id }, req.body, (err, doc) => {
        if (err) {
          console.log(err);
          res.status(500).json({
            error: err
          });
        }
        res.status(200).json([doc, { message: "updated successfully" }]);
      });
    }
  });
});

router.delete("/:id", checkAuth, (req, res, next) => {
  ClientUtil.deleteClient({ _id: req.params.id }, err => {
    if (err) {
      console.log(err);
      res.status(500).json({
        error: err
      });
    }
    res.status(200).json({ message: "deleted successfully" });
  });
});

module.exports = router;
