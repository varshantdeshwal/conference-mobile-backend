const Client = require("../models/client");

module.exports.addClient = function(query, callback) {
  Client.create(query, callback);
};
module.exports.checkClient = function(query, callback) {
  Client.find(query, callback);
};

module.exports.getClients = function(query, callback) {
  Client.find(query, callback);
};

module.exports.deleteClient = function(query, callback) {
  Client.deleteOne(query, callback);
};

module.exports.updateClient = function(query, update, callback) {
  Client.findOneAndUpdate(query, update, callback);
};
