const express = require("express");
const router = express.Router();
const LocationUtil = require("../utils/locationUtil");
const checkAuth = require("../auth/check-auth");

router.get("/:clientId", checkAuth, (req, res, next) => {
  LocationUtil.getLocations(
    {
      clientId: req.params.clientId
    },
    (err, doc) => {
      if (err) {
        console.log(err);
        res.status(500).json({
          error: err
        });
      }
      res.status(200).json(doc);
    }
  );
});

router.post("/", checkAuth, (req, res, next) => {
  LocationUtil.checkLocation(req.body, (err, doc) => {
    if (err) {
      console.log(err);
      res.status(500).json({
        error: err
      });
    } else if (doc.length >= 1) {
      res.status(400).json({ message: "location already exists" });
    } else {
      LocationUtil.addLocation(req.body, (err, doc) => {
        if (err) {
          console.log(err);
          res.status(500).json({
            error: err
          });
        }
        res.status(201).json([doc, { message: "location created" }]);
      });
    }
  });
});
router.put("/:id", checkAuth, (req, res, next) => {
  LocationUtil.checkLocation(req.body, (err, doc) => {
    if (err) {
      console.log(err);
      res.status(500).json({
        error: err
      });
    } else if (doc.length >= 1) {
      res.status(400).json({ message: "Location already exists" });
    } else {
      LocationUtil.updateLocation(
        { _id: req.params.id },
        req.body,
        (err, doc) => {
          if (err) {
            console.log(err);
            res.status(500).json({
              error: err
            });
          }
          res.status(200).json([doc, { message: "updated successfully" }]);
        }
      );
    }
  });
});
router.delete("/:id", checkAuth, (req, res, next) => {
  LocationUtil.deleteLocation({ _id: req.params.id }, err => {
    if (err) {
      console.log(err);
      res.status(500).json({
        error: err
      });
    }
    res.status(200).json({ message: "deleted successfully" });
  });
});
module.exports = router;
