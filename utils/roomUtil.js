const Room = require("../models/room");

module.exports.addRoom = function(query, callback) {
  Room.create(query, callback);
};
module.exports.checkRoom = function(query, callback) {
  Room.find(query, callback);
};

module.exports.getRooms = function(query, callback) {
  Room.find(query, callback);
};
module.exports.getSeatingCapacity = function(query, field, callback) {
  Room.find(query).distinct(field, callback);
};
module.exports.getRoomsWithCapacity = function(query, callback) {
  Room.find(query, callback);
};
module.exports.deleteRoom = function(query, callback) {
  Room.deleteOne(query, callback);
};

module.exports.updateRoom = function(query, update, callback) {
  Room.findOneAndUpdate(query, update, callback);
};
