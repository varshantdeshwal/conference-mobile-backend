var mongoose = require("mongoose");

const bookingSchema = mongoose.Schema({
  name:{type: String, required:true},
  email:{type: String, required:true},
  roomId: { type: String, required: true },
  slots: { type: [Number], required: true },
  date: { type: String, required: true },
  details: {
    client: {type:String, required:true},
    devcenter: {type:String, required:true},
    location: {type:String, required:true},
    seatingcap: {type:String, required:true},
    roomNumber: {type:Number, required:true}
  }
});

module.exports = mongoose.model("Booking", bookingSchema);
