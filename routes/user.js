const express = require("express");
const router = express.Router();
const User = require("../utils/user");

const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const checkAuth = require("../auth/check-auth");
router.post("/signup", (req, res) => {
  User.check({ email: req.body.email }, (err, doc) => {
    if (err) {
      res.json({ error: err });
    } else if (doc.length) {
      res.json({ message: "user exists" });
    } else {
      bcrypt.hash(req.body.password, 10, (err, hash) => {
        if (err) {
          return res.json({ error: err });
        } else {
          let data = {
            name: req.body.name,
            email: req.body.email,
            password: hash,
            status: req.body.status
          };
          User.add(data, (err, doc) => {
            if (err) {
              console.log(err);
              res.status(500).json({
                error: err
              });
            } else {
              res.json({ doc: doc, message: "signup successful" });
            }
          });
        }
      });
    }
  });
});
router.get("/check/:email",checkAuth, (req, res) => {
  User.check({ email: req.params.email }, (err, doc) => {
    if (err) {
      res.json({ error: err });
    } else if (doc.length) {
      res.json({ message: "user exists" });
    } else {
      res.json({ message: "doesn't exists" })
    }
  });
});

router.put("/change-password/:id", checkAuth, (req, res) => {
  let id = req.params.id;
  User.check({ _id: id }, (err, doc) => {
    if (err) {
      res.json({ error: err });
    } else if (doc.length < 1) {
      res.status(200).json({ message: "user doesn't exist" });
    } else {
      bcrypt.compare(req.body.oldPassword, doc[0].password, (err, result) => {
        if (err) {
          return res.status(200).json({ message: "auth failed" });
        } else if (result) {
          bcrypt.hash(req.body.newPassword, 10, (err, hash) => {
            if (err) {
              return res.json({ error: err });
            } else {
              User.edit(id, { password: hash }, { new: true }, (err, data) => {
                if (err) {
                  console.log(err);
                  res.status(500).json({
                    error: err
                  });
                } else {
                  res.json({ doc: data, message: "successful" });
                }
              });
            }
          });
        } else {
          return res.json({
            message: "wrong old password"
          });
        }
      });
    }
  });
});

router.post("/login", (req, res) => {
  User.check({ email: req.body.email }, (err, doc) => {
    if (err) {
      res.json({ error: err });
    } else if (doc.length < 1) {
      res.status(200).json({ message: "email doesn't exist" });
    } else {
      bcrypt.compare(req.body.password, doc[0].password, (err, result) => {
        if (err) {
          return res.status(200).json({ message: "auth failed" });
        } else if (result) {
          if (doc[0].status === "pending") {
            res.json({ message: "pending" });
          } else if (doc[0].status === "approved") {
            const token = jwt.sign(
              {
                email: doc[0].email,
                userId: doc[0]._id,
                name: doc[0].name
              },
              "decipher"
              // {
              //   expiresIn: "10h"
              // }
            );
            return res.status(200).json({
              message: "auth successful",
              token: token,
              payload: {
                userId: doc[0]._id,
                email: doc[0].email,
                name: doc[0].name
              }
            });
          }
        } else {
          return res.json({
            message: "wrong password"
          });
        }
      });
    }
  });
});
router.get("/pending", checkAuth, (req, res) => {
  User.pendingRequests({ status: "pending" }, (err, doc) => {
    if (err) {
      res.json({ error: err });
    } else {
      res.status(200).json(doc);
    }
  });
});
router.get("/active", checkAuth, (req, res) => {
  User.activeInterviewers(
    {
      status: "approved"
    },
    (err, doc) => {
      if (err) {
        res.json({ error: err });
      } else {
        res.status(200).json(doc);
      }
    }
  );
});
router.put("/:_id", checkAuth, (req, res) => {
  User.approveRequest(
    req.params._id,
    { status: "approved" },
    { new: true },
    (err, doc) => {
      if (err) {
        throw err;
      }
      res.json({ message: "approved", user: doc });
    }
  );
});
router.delete("/request/:_id", checkAuth, (req, res, next) => {
  User.declineRequest({ _id: req.params._id }, (err, doc) => {
    if (err) {
      throw err;
    }
    res.json({ message: "declined", user: doc });
  });
});
router.delete("/:_id", checkAuth, (req, res, next) => {
  User.deleteUser({ _id: req.params._id }, (err, doc) => {
    if (err) {
      throw err;
    }
    res.json({ message: "deleted", user: doc });
  });
});
router.get("/name/:id", checkAuth, (req, res) => {
  User.getName({ _id: req.params.id }, (err, doc) => {
    if (err) {
      res.json({ error: err });
    } else {
      res.status(200).json({ doc: doc[0], message: "successful" });
    }
  });
});
module.exports = router;
