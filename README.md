**Its a conference room booking application for mobile devices.**

**Demo** - https://vimeo.com/321214584

**This repository contains backend code for the application with following features -**

 1. Application related endpoints(APIs).
 2. User authentication using JSON WEB TOKEN (https://github.com/auth0/node-jsonwebtoken) with each API call. 
 3. User password encryption using BCRYPT (https://www.npmjs.com/package/bcrypt).
 4. Hosted database on mlab (https://mlab.com).
 
 
**NOTE** - In order to use your local database, change db url in ```app.js``` to ```mongodb://localhost:27017/<db-name>```.

**Tools and Technologies used :**


       - Mongo DB
       - Node JS
       - Express JS
	
**Local Setup -**

  1. Clone the repository.
  3. Run command "npm install" inside project root directory to install dependencies.
  4. Run command "node app.js" to run the application.


For any help, drop a mail to **varshant.14@gmail.com**