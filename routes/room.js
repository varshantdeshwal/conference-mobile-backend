const express = require("express");
const router = express.Router();
const RoomUtil = require("../utils/roomUtil");
const Room = require("../models/room");
const checkAuth = require("../auth/check-auth");

router.get("/:locationId", checkAuth, (req, res, next) => {
  RoomUtil.getRooms({ locationId: req.params.locationId }, (err, doc) => {
    if (err) {
      console.log(err);
      res.status(500).json({
        error: err
      });
    }
    res.status(200).json(doc);
  });
});
router.get("/seatingCapacity/:locationId", checkAuth, (req, res, next) => {
  RoomUtil.getSeatingCapacity(
    { locationId: req.params.locationId },
    "seatingCapacity",
    (err, doc) => {
      if (err) {
        console.log(err);
        res.status(500).json({
          error: err
        });
      }

      res.status(200).json(doc);
    }
  );
});
router.get(
  "/getRoomsWithCapacity/:locationId/:capacity",
  checkAuth,
  (req, res, next) => {
    RoomUtil.getRoomsWithCapacity(
      {
        locationId: req.params.locationId,
        seatingCapacity: req.params.capacity
      },
      (err, doc) => {
        if (err) {
          console.log(err);
          res.status(500).json({
            error: err
          });
        }

        res.status(200).json(doc);
      }
    );
  }
);

router.post("/", checkAuth, (req, res, next) => {
  RoomUtil.checkRoom(req.body, (err, doc) => {
    if (err) {
      console.log(err);
      res.status(500).json({
        error: err
      });
    } else if (doc.length >= 1) {
      res.status(400).json({ message: "room already exists" });
    } else {
      RoomUtil.addRoom(req.body, (err, doc) => {
        if (err) {
          console.log(err);
          res.status(500).json({
            error: err
          });
        }
        res.status(201).json([doc, { message: "room created" }]);
      });
    }
  });
});
router.delete("/:id", checkAuth, (req, res, next) => {
  Room.remove({ _id: req.params.id })
    .then(result => {
      res.status(201).json({ message: "room deleted" });
    })
    .catch(err => {
      console.log(err);
      res.status(404).json({
        error: err
      });
    });
});

router.put("/:id", checkAuth, (req, res, next) => {
  RoomUtil.checkRoom(req.body, (err, doc) => {
    if (err) {
      console.log(err);
      res.status(500).json({
        error: err
      });
    } else if (doc.length >= 1) {
      res.status(400).json({ message: "room already exists" });
    } else {
      RoomUtil.updateRoom({ _id: req.params.id }, req.body, (err, doc) => {
        if (err) {
          console.log(err);
          res.status(500).json({
            error: err
          });
        }
        res.status(200).json([doc, { message: "updated successfully" }]);
      });
    }
  });
});

module.exports = router;
