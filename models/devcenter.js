var mongoose = require("mongoose");

const devcenterSchema = mongoose.Schema({
  name: { type: String, required: true }
});

module.exports = mongoose.model("Devcenter", devcenterSchema);
