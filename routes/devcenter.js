const express = require("express");
const router = express.Router();
const checkAuth = require("../auth/check-auth");

const DevcenterUtil = require("../utils/devcenterUtil");
router.get("/", checkAuth, (req, res, next) => {
  DevcenterUtil.getDevcenters({}, (err, doc) => {
    if (err) {
      console.log(err);
      res.status(500).json({
        error: err
      });
    }
    res.status(200).json(doc);
  });
});

router.post("/", checkAuth, (req, res, next) => {
  var devcenter = { name: req.body.name };
  DevcenterUtil.checkDevcenter(devcenter, (err, doc) => {
    if (err) {
      console.log(err);
      res.status(500).json({
        error: err
      });
    } else if (doc.length >= 1) {
      res.status(400).json({ message: "development center already exists" });
    } else {
      DevcenterUtil.addDevcenter(devcenter, (err, doc) => {
        if (err) {
          console.log(err);
          res.status(500).json({
            error: err
          });
        }
        res.status(201).json([doc, { message: "devcenter created" }]);
      });
    }
  });
});

router.put("/:id", checkAuth, (req, res, next) => {
  DevcenterUtil.checkDevcenter({ name: req.body.name }, (err, doc) => {
    if (err) {
      console.log(err);
      res.status(500).json({
        error: err
      });
    } else if (doc.length >= 1) {
      res.status(400).json({ message: "development center already exists" });
    } else {
      DevcenterUtil.updateDevcenter(
        { _id: req.params.id },
        req.body,
        (err, doc) => {
          if (err) {
            console.log(err);
            res.status(500).json({
              error: err
            });
          }
          res.status(200).json([doc, { message: "updated successfully" }]);
        }
      );
    }
  });
});
router.delete("/:id", checkAuth, (req, res, next) => {
  DevcenterUtil.deleteDevcenter({ _id: req.params.id }, err => {
    if (err) {
      console.log(err);
      res.status(500).json({
        error: err
      });
    }
    res.status(200).json({ message: "deleted successfully" });
  });
});

module.exports = router;
