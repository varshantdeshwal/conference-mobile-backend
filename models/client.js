var mongoose = require("mongoose");

const clientSchema = mongoose.Schema({
  name: { type: String, required: true },
  devId: { type: String, required: true }
});

module.exports = mongoose.model("Client", clientSchema);
